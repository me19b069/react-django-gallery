import './css/App.css';
import './navbar/nav.css';
import Navigation from './navbar/Navbar';
import {BrowserRouter as Router,Switch,Route, Redirect} from "react-router-dom";
import Home from './components/home/Home';
import About from './components/about/About';
import Services from './components/services/Services.js';
import Contact from './components/contact/Contact';
import Aurthorisation from './components/aurthorisation/aurthorisation';
import Gallery from './components/gallery/Gallery';
import Blog from './components/blog/blog';
import Dashboard from './components/dashboard';


// check localStorage
sessionStorage.setItem("isLoggedIn",localStorage.getItem('isLoggedIn'));
sessionStorage.setItem("token",localStorage.getItem('token'));
//window.isLoggedIn=true;
function AdminApp() {

  
  return (
    <Router>
      {/* <Navigation/> */}
        <Navigation/>
        <Switch >
          <Route path="/admin/aurthorise">
            <Aurthorisation/>
          </Route>
          <Route path="/admin/gallery">
            <Gallery/>
          </Route>
          <Route path="/admin/blogs">
            <Blog/>
          </Route>
          <Route path="/admin/dashboard">
            <Dashboard/>
          </Route>
          <Redirect to="/"/>
        </Switch>
      
      </Router>
  );
}

export default AdminApp;
