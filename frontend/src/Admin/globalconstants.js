import React from 'react';

const defaultvalue={
    isLoggedIn:false,
    token:""
}

const GlobalContext = React.createContext(defaultvalue);
export default GlobalContext;
