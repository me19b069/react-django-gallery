import React, { Component } from 'react'
import { Navbar, Nav ,NavDropdown } from 'react-bootstrap';
function Navigation(){
    return (
            <Navbar collapseOnSelect expand="lg" className="navbar fixed-top" variant="dark">
                <Navbar.Brand className="nav-brand" href="/">logo</Navbar.Brand>
                <Navbar.Toggle id="menu" className="nav-toggler" aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                    <Nav.Link href="/admin/dashboard">dashboard</Nav.Link>
                    <Nav.Link href="/admin/gallery">Gallery</Nav.Link>
                    <Nav.Link href="/admin/blogs">Blogs</Nav.Link> 
                    <Nav.Link href="/admin/users">User</Nav.Link>       
                    </Nav>
                </Navbar.Collapse>
                </Navbar>
        )
    
}

export default Navigation
