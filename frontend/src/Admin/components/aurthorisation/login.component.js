import React, { Component } from "react";
import axios from "axios";
import AuthRedirect from "./redirect";

export default class Login extends Component {
    constructor() {
        super();
        this.state = {
            username: "",
            password: "",
            message: "",
            isAdmin:false,
            remember: false,
            isLoggedIn:window.isLoggedIn,
        };

        this.handleUserNameChange = this.handleUserNameChange.bind(this);
        this.login = this.login.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleRememberChange= this.handleRememberChange.bind(this);
        this.handleIsAdminChange=this.handleIsAdminChange.bind(this);
    }

    handleUserNameChange(e) {
        e.preventDefault();
        this.setState({
            username: e.target.value,
        });
    }
    handlePasswordChange(e) {
        e.preventDefault();
        this.setState({
            password: e.target.value
        });
    }

    login(e) {
        e.preventDefault();
        axios
            .post("/api/login/", this.state)
            .then((response) => {
                   const data=response
                   console.log(response)
                   if(data.status===200) {
                       window.isLoggedIn=true
                       window.token=data.data.token

                       // session
                       
                        if(this.state.remember){
                            localStorage.setItem("isLoggedIn",true)
                            localStorage.setItem("token",data.data.token)
                        }
                       //session
                       sessionStorage.setItem("isLoggedIn",true)
                       sessionStorage.setItem("token",data.data.token)

                       console.log(localStorage)
                       this.setState({
                           isLoggedIn:true
                       })
                        console.log(this.state)

                       //set token to local storage is remember is enabled

                   }
                   
            })
            .catch((err) => {
                
               this.setState({
                   message:"wrong user name or password"
               })
            });
    }

    handleRememberChange(e) {
        //e.preventDefault();
        this.setState({
            remember: e.target.value
        });
    }
    handleIsAdminChange(e) {
            //e.preventDefault();
            this.setState({
                isAdmin: e.target.value
            })
        }

    render() {
            if(window.isLoggedIn){
                return (
                    <AuthRedirect to="/"/>
                )
            }
        return (
            <form className="form-auth">
                <h3>Sign In</h3>
                {!this.state.message ? (
                    " "
                ) : (
                    <div className="form-group">
                        <label>{this.state.message}</label>
                    </div>
                )}

                <div className="form-group">
                    <label>User Name</label>
                    <input
                        type="username"
                        className="form-control"
                        placeholder="Enter username"
                        value={this.state.username}
                        onChange={this.handleUserNameChange}
                    />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input
                        type="password"
                        className="form-control"
                        placeholder="Enter password"
                        value={this.state.password}
                        onChange={this.handlePasswordChange}
                    />
                </div>

                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="customCheck1"
                            value={this.state.remember}
                            onChange={this.handleRememberChange}
                        />
                        <label className="custom-control-label" htmlFor="customCheck1">
                            Remember me
            </label>
                    </div>
                </div>
                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="customCheck2"
                            value={this.state.isAdmin}
                            onChange={this.handleIsAdminChange}
                        />
                        <label className="custom-control-label" htmlFor="customCheck2">
                            Login As administrator
            </label>
                    </div>
                </div>

                <button onClick={this.login} className="btn btn-primary btn-block">
                    Submit
        </button>
                <p className="forgot-password text-right">
                    Forgot <a href="/">password?</a>
                </p>
            </form>
        );
    }
}
