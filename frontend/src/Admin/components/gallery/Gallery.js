import React from 'react';
import "./gallery.css";
import data from "./data.js";


class Gallery extends React.Component {
  constructor(props) {
    super(props);
    this.state = { currentIndex: null };
    this.closeModal = this.closeModal.bind(this);
    this.findNext = this.findNext.bind(this);
    this.findPrev = this.findPrev.bind(this);
    this.renderImageContent = this.renderImageContent.bind(this);
  }
  renderImageContent(object) {
    return (
      <div onClick={(e) => this.openModal(e, object.id)}>
        <img src={object.src} key={object.id} alt="this" />
      </div>
    ) 
  }


  openModal(e, index) {
     // e.preventDefault();
    this.setState ({ currentIndex: index });
    console.log(index);
  }


  closeModal(e) {
    if (e !== undefined) {
      e.preventDefault();
    }
    this.setState ({ currentIndex: null });
  }

  findPrev(e) {
    if (e !== undefined) {
      e.preventDefault();
    }
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex -1
    }));
  }
  findNext(e) {
    if (e !== undefined) {
      e.preventDefault();
    }
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1
    }));
  }
  render() {
    return (
      <div className="gallery-container">
        <h1>Gallery</h1>
        <div className="gallery-grid">
          {data.map(this.renderImageContent)}
        </div>
        <GalleryModal 
          closeModal={this.closeModal} 
          findPrev={this.findPrev} 
          findNext={this.findNext} 
          hasPrev={this.state.currentIndex >0} 
          hasNext={this.state.currentIndex < data.length} 
          img={data[this.state.currentIndex-1]} 
        />
      </div>
    )
  }
}


class GalleryModal extends React.Component {
    constructor() {
      super();
      this.handleKeyDown = this.handleKeyDown.bind(this);
    }
  
    componentDidMount() {
      document.body.addEventListener('keydown', this.handleKeyDown);
    }  
    componentWillUnmount() {
      document.body.removeEventListener('keydown', this.handleKeyDown);
    }
    handleKeyDown(e) {
      if (e.keyCode === 27)
        this.props.closeModal();
      if (e.keyCode === 37 && this.props.hasPrev)
        this.props.findPrev();
      if (e.keyCode === 39 && this.props.hasNext)
        this.props.findNext();
    }

    
    render () {
      const { closeModal, hasNext, hasPrev, findNext, findPrev, img } = this.props;
      if (!img) {
        console.log(this.props)
        return null;
      }
      return (





          <div className="gallery-model">
              <div className="gallery-model-container">
              <div className="gallery-model-title"><h2 >{img.name}</h2></div>
              <div className="gallery-model-body">
              <div className="gallery-model-icons">
              <a href="/" className='gallery-model-close' onClick={closeModal} onKeyDown={this.handleKeyDown}>X</a>
               {hasPrev && <a href="/" className='gallery-model-prev' onClick={findPrev} onKeyDown={this.handleKeyDown}>&lsaquo;</a>}
               {hasNext && <a href="/" className='gallery-model-next' onClick={findNext} onKeyDown={this.handleKeyDown}>&rsaquo;</a>}
                </div> 
                    <img className="rounded gallery-model-image" src={img.src} alt="alterne" />
              </div>
              <div className="gallery-model-description">{img.description}</div>
              </div>
          </div>
      )
    }
  }



//ReactDOM.render(<Gallery />, document.querySelector('.gallery-container'));

export default Gallery;




