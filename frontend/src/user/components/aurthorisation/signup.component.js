import axios from "axios";
import React, { Component } from "react";
import {browserhistory} from "react-router";
import AuthRedirect from "./redirect";
export default class SignUp extends Component {
        constructor(props) {
            super(props);
            this.state = {
                    username:"",
                    email:"",
                    password:"",
                    isLoggedIn:window.isLoggedIn,
                    err:false,
                    message:""
            }

            this.handleEmailChange= this.handleEmailChange.bind(this);
            this.handlePasswordChange=this.handlePasswordChange.bind(this);
            this.handleUserNameChange= this.handleUserNameChange.bind(this);
            this.register= this.register.bind(this);
        }

        handleUserNameChange(e) {
            e.preventDefault();
            this.setState({
                username: e.target.value,
            });
        }
        handleEmailChange(e) {
            e.preventDefault();
            this.setState({
                email: e.target.value,
            });
        }
        handlePasswordChange(e) {
            e.preventDefault();
            this.setState({
                password: e.target.value
            });
        }
    

register(e){
    e.preventDefault();
    const user= this.state
    // do all checks

    // send request to register user
    axios.post("api/register/",user).then((response) => {
                    if(response.data.status ===200){
                        window.isLoggedIn=true;
                        window.token=response.data.token
                        this.setState({
                            message:"sucessfully registered",
                            isLoggedIn:true,
                        })

                    }

                    
    }).catch((error) => {
            console.log(error.response.data)
    })




}

    render() {
        if(window.isLoggedIn){
            window.alert(this.state.message);
            return (
                <AuthRedirect to="/userpanel"/>
            )
        }
        return (
            <form className='form-auth'>
                <h3>Sign Up</h3>

                <div className="form-group">
                    <label> username(* should be unique)</label>
                    <input type="text" className="form-control" placeholder="username" value={this.state.username} onChange={this.handleUserNameChange}/>
                </div>

                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" placeholder="Enter email"  value={this.state.email} onChange={this.handleEmailChange}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" value={this.state.password} onChange={this.handlePasswordChange}/>
                </div>

                <button  className="btn btn-primary btn-block" onClick={this.register}>Sign Up</button>
                <p className="forgot-password text-right">
                    Already registered <a href="/">sign in?</a>
                </p>
            </form>
        );
    }
}