import React, { Component } from 'react'
import "./userinfo.css";
import axios from "axios";
class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            user_id:this.props.user_id,
            profile: "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg",
            age: "23",
            country: "",
            city: "",
            profession: "",
            address: "",
            postalcode: "",
            bio: "",
        }
        this.handleEmailChange= this.handleEmailChange.bind(this);
        this.handleNameChange= this.handleNameChange.bind(this);
        this.handleAddressChange= this.handleAddressChange.bind(this);
        this.handleBioChange=this.handleBioChange.bind(this);
        this.handlePostalCodeChange= this.handlePostalCodeChange.bind(this);
        this.handleCountryChange= this.handleCountryChange.bind(this);
        this.handleCityChange=this.handleCityChange.bind(this);
        this.updateuserinfo= this.updateuserinfo.bind(this);
    }
    // function for handeling name change
    handleNameChange(e) {
        e.preventDefault();
        this.setState({
            name: e.target.value
        })
    }
    handleEmailChange(e) {
        e.preventDefault();
        this.setState({
            email: e.target.value
        })
    }
    handleAddressChange(e){
        e.preventDefault();
        this.setState({
            address:e.target.value
        })
    }

    handleCityChange(e){
        e.preventDefault();
        this.setState({
            city:e.target.value
        })
    }
    handleCountryChange(e){
        e.preventDefault();
        this.setState({
                country: e.target.value
        })
    }
    handlePostalCodeChange(e){
        e.preventDefault();
        this.setState({
            postalcode:e.target.value
        })
    }

    handleBioChange(e){
        e.preventDefault();
        this.setState({
            bio:e.target.value
        })
    }

// fetch user information
componentDidMount(){
    axios.get("/user/"+this.state.user_id).then(response => {
            if (response.data.httpstatus===200) {
                this.setState({
                    name:response.data.data.name,
                    email:response.data.data.email,
                    bio:response.data.data.bio,
                    address:response.data.data.address

                })
            }
    }).catch(err => {
        console.log(err)
    })
}



updateuserinfo(e){
        e.preventDefault()
        // validate all of the user information


        // create a request to update the user info 
        const userdata =JSON.parse( this.state)



        axios.put("/user/"+this.state.user_id
                    ,userdata
                    ,{
                        headers:{ 'Content-Type': 'application/json',
                    "Authorization":window.token}
                    }).then(response => {
                        if(response.httpstatus===200){
                            console.log("information updated")
                        }
                    }).catch(err =>{
                        console.log(err)
                    })
}

    render() {
        return (
            <div>
                <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
                <div className="main-content">
                    {/* Header */}
                    <div className="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style={{ minHeight: '600px', backgroundImage: 'url(https://raw.githack.com/creativetimofficial/argon-dashboard/master/assets/img/theme/profile-cover.jpg)', backgroundSize: 'cover', backgroundPosition: 'center top' }}>
                        {/* Mask */}
                        <span className="mask bg-gradient-default opacity-8" />
                        {/* Header container */}
                        <div className="container-fluid d-flex align-items-center">
                            <div className="row">
                                <div className="col-lg-7 col-md-10">
                                    <h1 className="display-2 text-white">Hello {this.state.name}</h1>
                                    <p className="text-white mt-0 mb-5">This is your profile page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>
                                    <a href="#!" className="btn btn-info">Edit profile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Page content */}
                    <div className="container-fluid mt--7">
                        <div className="row">
                            <div className="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                                <div className="card card-profile shadow">
                                    <div className="row justify-content-center">
                                        <div className="col-lg-3 order-lg-2">
                                            <div className="card-profile-image">
                                                <a href="#!">
                                                    <img src={this.state.profile} className="rounded-circle" alt="profile" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                                        <div className="d-flex justify-content-between">
                                            <a href="#" className="btn btn-sm btn-info mr-4">Connect</a>
                                            <a href="#" className="btn btn-sm btn-default float-right">Message</a>
                                        </div>
                                    </div>
                                    <div className="card-body pt-0 pt-md-4">
                                        <div className="row">
                                            <div className="col">
                                                <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                                                    <div>
                                                        <span className="heading">22</span>
                                                        <span className="description">Friends</span>
                                                    </div>
                                                    <div>
                                                        <span className="heading">10</span>
                                                        <span className="description">Photos</span>
                                                    </div>
                                                    <div>
                                                        <span className="heading">89</span>
                                                        <span className="description">Comments</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="text-center">
                                            <h3>
                                                {this.state.name}<span className="font-weight-light">, {this.state.age}</span>
                                            </h3>
                                            <div className="h5 font-weight-300">
                                                <i className="ni location_pin mr-2" />{this.state.city + "  " + this.state.country}
                                            </div>
                                            <div className="h5 mt-4">
                                                <i className="ni business_briefcase-24 mr-2" />{this.state.profession}
                                            </div>
                                            {/* <div>
                                                <i className="ni education_hat mr-2" />University of Computer Science
                </div> */}
                                            <hr className="my-4" />
                                            <p>{this.state.about}</p>
                                            <a href="#">Show more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-8 order-xl-1">
                                <div className="card bg-secondary shadow">
                                    <div className="card-header bg-white border-0">
                                        <div className="row align-items-center">
                                            <div className="col-8">
                                                <h3 className="mb-0">My account</h3>
                                            </div>
                                            <div className="col-4 text-right">
                                                <a href="#!" className="btn btn-sm btn-primary">Settings</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <form>
                                            <h6 className="heading-small text-muted mb-4">User information</h6>
                                            <div className="pl-lg-4">
                                                <div className="row">
                                                    <div className="col-lg-6">
                                                        <div className="form-group focused">
                                                            <label className="form-control-label" htmlFor="input-username">Username</label>
                                                            <input type="text" id="input-username" className="form-control form-control-alternative" placeholder="Username" value={this.state.name} onChange={this.handleNameChange}/>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-6">
                                                        <div className="form-group">
                                                            <label className="form-control-label" htmlFor="input-email">Email address</label>
                                                            <input type="email" id="input-email" className="form-control form-control-alternative" placeholder="youemail@example.com" value={this.state.email} onChange={this.handleEmailChange}/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-lg-6">
                                                        <div className="form-group focused">
                                                            <label className="form-control-label" htmlFor="input-first-name">First name</label>
                                                            <input type="text" id="input-first-name" className="form-control form-control-alternative" placeholder="First name" value={this.state.name} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-6">
                                                        <div className="form-group focused">
                                                            <label className="form-control-label" htmlFor="input-last-name">Last name</label>
                                                            <input type="text" id="input-last-name" className="form-control form-control-alternative" placeholder="Last name" value={this.state.name} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr className="my-4" />
                                            {/* Address */}
                                            <h6 className="heading-small text-muted mb-4">Contact information</h6>
                                            <div className="pl-lg-4">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="form-group focused">
                                                            <label className="form-control-label" htmlFor="input-address">Address</label>
                                                            <input id="input-address" className="form-control form-control-alternative" placeholder="Home Address" value={this.state.address} onChange={this.handleAddressChange} type="text" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-lg-4">
                                                        <div className="form-group focused">
                                                            <label className="form-control-label" htmlFor="input-city">City</label>
                                                            <input type="text" id="input-city" className="form-control form-control-alternative" placeholder="City" value={this.state.city} onChange={this.handleCityChange} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-4">
                                                        <div className="form-group focused">
                                                            <label className="form-control-label" htmlFor="input-country">Country</label>
                                                            <input type="text" id="input-country" className="form-control form-control-alternative" placeholder="Country" value={this.state.country} onChange={this.handleCountryChange} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-4">
                                                        <div className="form-group">
                                                            <label className="form-control-label" htmlFor="input-country">Postal code</label>
                                                            <input type="number" id="input-postal-code" className="form-control form-control-alternative" placeholder="Postal code" value={this.state.postalcode} onChange={this.handlePostalCodeChange} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr className="my-4" />
                                            {/* Description */}
                                            <h6 className="heading-small text-muted mb-4">About me</h6>
                                            <div className="pl-lg-4">
                                                <div className="form-group focused">
                                                    <label>About Me</label>
                                                    <textarea rows={4} className="form-control form-control-alternative" placeholder="A few words about you ..." value={this.state.bio}  onChange={this.handleBioChange}/>
                                                </div>
                                            </div>

                                            <div className="row align-items-center">
                                            <div className="col-8">
                                                <h3 className="mb-0">Save this information</h3>
                                            </div>
                                            <div className="col-4 text-right">
                                                <a href="#!" className="btn btn-sm btn-primary" onClick={this.updateuserinfo}>Save</a>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default UserInfo;
