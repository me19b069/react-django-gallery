import React, { Component } from 'react'

import im1 from "./assets/images/team/team-1.jpg";
import im2 from "./assets/images/team/team-2.jpg";
import im3 from "./assets/images/team/team-3.jpg";

class Team extends Component {
 mystyle = {
            align: 'center'
      };
    render() {
        return (
            <>      
            <h2 style={this.mystyle}>Our Team</h2>
            <div className="row">
            <div className="column">
            <div className="card">
            <img src={im3} alt="Jane" className="card-image"/>
            <div className="container description">
                <h2>Jane Doe</h2>
                <p className="title">CEO & Founder</p>
                <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                <p>jane@example.com</p>
                <p><button className="button">Contact</button></p>
            </div>
            </div>
        </div>

        <div className="column">
            <div className="card">
            <img src={im1} alt="Mike"className="card-image"/>
            <div className="container description">
                <h2>Mike Ross</h2>
                <p className="title">Art Director</p>
                <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                <p>mike@example.com</p>
                <p><button className="button">Contact</button></p>
            </div>
            </div>
        </div>

        <div className="column">
            <div className="card">
            <img src={im2} alt="John" className="card-image"/>
            <div className="container description">
                <h2>John Doe</h2>
                <p className="title">Designer</p>
                <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                <p>john@example.com</p>
                <p><button className="button">Contact</button></p>
            </div>
            </div>
        </div>
        </div>
        </>
        )
    }
}

export default Team
