import './css/App.css';
import './navbar/nav.css';
import Navigation from './navbar/Navbar';
import {BrowserRouter as Router,Switch,Route, Redirect} from "react-router-dom";
import Home from './components/home/Home';
import About from './components/about/About';
import Services from './components/services/Services.js';
import Contact from './components/contact/Contact';
import Aurthorisation from './components/aurthorisation/aurthorisation';
import Gallery from './components/gallery/Gallery';
import UserPanel from './components/userpanel/UserPanel';
import Blog from './components/blog/blog';


// check localStorage
sessionStorage.setItem("isLoggedIn",localStorage.getItem('isLoggedIn'));
sessionStorage.setItem("token",localStorage.getItem('token'));
//window.isLoggedIn=true;
function UserApp() {
  return (
    <Router>
        <Navigation/>
        <Switch >
          <Route path="/about">
            <About/>
          </Route>
          <Route path="/service">
            <Services/>
          </Route>
          <Route path="/contact">
            <Contact/>
          </Route>
          <Route path="/aurthorise">
            <Aurthorisation/>
          </Route>
          <Route path="/gallery">
            <Gallery/>
          </Route>
          <Route path="/blogs">
            <Blog/>
          </Route>
          <Route path="/userpanel">
            <UserPanel/>
          </Route>
          <Route exact path="/">
            <Home/>
          </Route>

          <Redirect to="/"/>
        </Switch>
      
      </Router>
  );
}

export default UserApp;
