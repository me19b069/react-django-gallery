import React, { Component } from 'react'

export class Page extends Component {
    render() {
        return (
            <div>
                <h1>404  the page which you are looking for is not found</h1>
            </div>
        )
    }
}

export default Page
