import React from 'react';
import ReactDOM from 'react-dom';
import 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import UserApp from './user/UserApp.js';
import {BrowserRouter as Router,Switch,Route, Redirect} from "react-router-dom";
import AdminApp from './Admin/AdminApp.js';
import Page from './404.js';

class Index extends React.Component{

    render () {
            return (
                <Router>
                
                <Switch >
                <Route path="/admin">
                    <AdminApp />
                </Route>
                  <Route exact path="/">
                    <UserApp/>
                  </Route>
                </Switch>
              </Router> 
            )

    }



}


ReactDOM.render(
    <Index/>
, document.getElementById('root')
);


