from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User



class Blog(models.Model):
	title= models.CharField(max_length=100)
	views=models.IntegerField()
	author=models.CharField(max_length=50)
	description=models.TextField() # used for multiline as of post
	image= models.ImageField(null=True,blank=True,upload_to="blog_images/") # ondelete means post will be deleted if user deletes his account

	def __str__(self): # used so as to return post title when we use shell to view the post from database
		return self.title+self.views+self.image